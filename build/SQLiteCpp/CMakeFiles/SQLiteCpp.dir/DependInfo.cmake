# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/ubuntu/Projects/splitted/SQLiteCpp/src/Backup.cpp" "/home/ubuntu/Projects/splitted/build/SQLiteCpp/CMakeFiles/SQLiteCpp.dir/src/Backup.cpp.o"
  "/home/ubuntu/Projects/splitted/SQLiteCpp/src/Column.cpp" "/home/ubuntu/Projects/splitted/build/SQLiteCpp/CMakeFiles/SQLiteCpp.dir/src/Column.cpp.o"
  "/home/ubuntu/Projects/splitted/SQLiteCpp/src/Database.cpp" "/home/ubuntu/Projects/splitted/build/SQLiteCpp/CMakeFiles/SQLiteCpp.dir/src/Database.cpp.o"
  "/home/ubuntu/Projects/splitted/SQLiteCpp/src/Exception.cpp" "/home/ubuntu/Projects/splitted/build/SQLiteCpp/CMakeFiles/SQLiteCpp.dir/src/Exception.cpp.o"
  "/home/ubuntu/Projects/splitted/SQLiteCpp/src/Statement.cpp" "/home/ubuntu/Projects/splitted/build/SQLiteCpp/CMakeFiles/SQLiteCpp.dir/src/Statement.cpp.o"
  "/home/ubuntu/Projects/splitted/SQLiteCpp/src/Transaction.cpp" "/home/ubuntu/Projects/splitted/build/SQLiteCpp/CMakeFiles/SQLiteCpp.dir/src/Transaction.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "SQLITE_ENABLE_COLUMN_METADATA"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../SQLiteCpp/include"
  "../SQLiteCpp/sqlite3"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
