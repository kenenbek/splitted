//
// Created by kenenbek on 27.03.17.
//

#include <yaml-cpp/yaml.h>
#include <SQLiteCpp/SQLiteCpp.h>
#include <iostream>
#include "databasecreater.h"


using namespace std;

#ifdef SQLITECPP_ENABLE_ASSERT_HANDLER
namespace SQLite
{
/// definition of the assertion handler enabled when SQLITECPP_ENABLE_ASSERT_HANDLER is defined in the project (CMakeList.txt)
void assertion_failed(const char* apFile, const long apLine, const char* apFunc, const char* apExpr, const char* apMsg)
{
    // Print a message to the standard error output stream, and abort the program.
    std::cerr << apFile << ":" << apLine << ":" << " error: assertion failed (" << apExpr << ") in " << apFunc << "() with message \"" << apMsg << "\"\n";
    std::abort();
}
}
#endif


std::string vector_to_string(std::vector<std::string>& s_vector){
    std::string new_string = "";
    size_t len = s_vector.size();
    for (int i = 0; i < len; ++i) {
        new_string += s_vector[i];
        new_string += "-";
    }
    return new_string;
}

int CREATE_BKK_DATABASE(std::string& path_to_bkk){

    // Open a database file in create/write mode
    SQLite::Database db("bkk.db3", SQLite::OPEN_READWRITE | SQLite::OPEN_CREATE);
    std::cout << "SQLite database file '" << db.getFilename().c_str() << "' opened successfully\n";

    // Create a new table with an explicit "id" column aliasing the underlying rowid
    db.exec("DROP TABLE IF EXISTS bkk");

    db.exec("CREATE TABLE bkk (id TEXT PRIMARY KEY, '#events' TEXT, \
                    BKKPath TEXT, \
                    DataQuality TEXT, \
                    Replica TEXT, \
                    RunNumber TEXT, \
                    Size float, \
                    Storages TEXT)");

    return 0;
    }

int INSERT_BKK_DATABASE(SQLite::Database& db, std::string path_to_bkk){
    std::cout << "start insert: " << path_to_bkk << std::endl;

    int failures = 0;
    int success = 0;

    YAML::Node root = YAML::LoadFile(path_to_bkk);
    std::cout << "loaded" << std::endl;



    for (auto it = root.begin(); it != root.end(); ++it) {
        char SQL_STATEMENT[100000];

        std::vector<std::string> my_storage_vector;
        std::vector<std::string> my_bkk_vector;

        try {
            my_storage_vector = it->second["Storages"].as<std::vector<std::string>>();
            my_bkk_vector = it->second["BKKPath"].as<std::vector<std::string>>();
        }catch (YAML::BadConversion& badConversion){
            failures++;
            continue;
        }

        std::string Storages = vector_to_string(my_storage_vector);
        std::string BKKPath = vector_to_string(my_bkk_vector);

        try{
            sprintf(SQL_STATEMENT, "INSERT INTO bkk VALUES "
                            "(\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", %f, \"%s\")",
                    it->first.as<std::string>().c_str(),
                    it->second["#events"].as<std::string>().c_str(),
                    BKKPath.c_str(),
                    it->second["DataQuality"].as<std::string>().c_str(),
                    it->second["Replica"].as<std::string>().c_str(),
                    it->second["RunNumber"].as<std::string>().c_str(),
                    it->second["Size"].as<std::string>().compare("None") != 0 ? it->second["Size"].as<float>() : 0,
                    Storages.c_str()
            );
        }
        catch (YAML::BadConversion& badConversion){
            std::cout << badConversion.what() << std::endl;
            std::cout << badConversion.msg << std::endl << std::endl;
            failures++;
            continue;
        }
        //std::cout << SQL_STATEMENT << std::endl;

        try {
            db.exec(SQL_STATEMENT);
            ++success;
        }
        catch (std::exception& e) {
            std::cout << "SQLite exception: " << e.what() << std::endl;
            std::cout << it->first.as<std::string>() << std::endl;
            continue;
        }
    }

    std::cout << "failures: " << failures << std::endl;
    std::cout << "success: " << success << std::endl;


    return 0;

}

int CREATE_JOBS_DATABASE(){
    try {
        // Open a database file in create/write mode
        SQLite::Database db("jobs.db3", SQLite::OPEN_READWRITE | SQLite::OPEN_CREATE);
        std::cout << "SQLite database file '" << db.getFilename().c_str() << "' opened successfully\n";

        // Create a new table with an explicit "id" column aliasing the underlying rowid
        db.exec("DROP TABLE IF EXISTS jobs");

        db.exec("CREATE TABLE jobs (id INTEGER PRIMARY KEY, EndExecTime FLOAT, \
                        Federation TEXT, \
                        InputFiles TEXT, \
                        JobGroup TEXT, \
                        JobType TEXT, \
                        LocalJobID TEXT, \
                        OutputFiles TEXT, \
                        RescheduleTime TEXT, \
                        Site TEXT, \
                        StartExecTime FLOAT, \
                        Status TEXT, \
                        SubmissionTime FLOAT, \
                        SystemPriority FLOAT, \
                        'TotalCPUTime(s)' FLOAT, \
                        UserPriority FLOAT, \
                        'WallClockTime(s)' FLOAT)");

    }catch (std::exception& e) {
        std::cout << "SQLite exception: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return 0;
}


int INSERT_JOBS_DATABASE(SQLite::Database& db, std::string& jobs_yaml_path){

    int failures = 0;
    int success = 0;

    YAML::Node root = YAML::LoadFile(jobs_yaml_path);

    for (auto it = root.begin(); it != root.end(); ++it) {
        char SQL_STATEMENT[100000];

        std::vector<std::string> my_in_vector;
        std::vector<std::string> my_out_vector;

        try {
            my_in_vector = it->second["InputFiles"].as<std::vector<std::string>>();
            my_out_vector = it->second["OutputFiles"].as<std::vector<std::string>>();
        }catch (YAML::BadConversion& badConversion){
            failures++;
            continue;
        }

        std::string input = vector_to_string(my_in_vector);
        std::string output = vector_to_string(my_out_vector);


        try{
            sprintf(SQL_STATEMENT, "INSERT INTO jobs VALUES "
                            "(%d, %f, \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", %f, \"%s\", %f, %f, %f, %f, %f)",
                    it->first.as<int>(), it->second["EndExecTime"].as<float>(),
                    it->second["Federation"].as<std::string>().c_str(),

                    input.c_str(),

                    it->second["JobGroup"].as<std::string>().c_str(),
                    it->second["JobType"].as<std::string>().c_str(),
                    it->second["LocalJobID"].as<std::string>().c_str(),

                    output.c_str(),

                    it->second["RescheduleTime"].as<std::string>().c_str(),
                    "SITE",
                    it->second["StartExecTime"].as<float>(),
                    it->second["Status"].as<std::string>().c_str(),
                    it->second["SubmissionTime"].as<float>(),
                    it->second["SystemPriority"].as<float>(),
                    it->second["TotalCPUTime(s)"].as<float>(),
                    it->second["UserPriority"].as<float>(),
                    it->second["WallClockTime(s)"].as<float>()
            );
        } catch (YAML::BadConversion& badConversion){
            std::cout << badConversion.what() << std::endl;
            std::cout << badConversion.msg << std::endl << std::endl;
            failures++;
            continue;
        }
        try {
            db.exec(SQL_STATEMENT);
            ++success;
        }catch (std::exception& e) {
            std::cout << "SQLite exception: " << e.what() << std::endl;
            std::cout << it->first.as<std::string>() << std::endl;
            continue;
        }
    }

    std::cout << "failures: " << failures << std::endl;
    std::cout << "success: " << success << std::endl;

    return 0;
}