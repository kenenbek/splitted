//
// Created by kenenbek on 27.03.17.
//

#ifndef C_DATABASEEXAMPLE_DATABASECREATER_H
#define C_DATABASEEXAMPLE_DATABASECREATER_H

int CREATE_JOBS_DATABASE();
int INSERT_JOBS_DATABASE(SQLite::Database& db, std::string& jobs_yaml_path);


int CREATE_BKK_DATABASE(std::string& path_to_bkk);
int INSERT_BKK_DATABASE(SQLite::Database& db, std::string path_to_bkk);

#endif //C_DATABASEEXAMPLE_DATABASECREATER_H
