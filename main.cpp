#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <yaml-cpp/yaml.h>
#include <vector>
#include <SQLiteCpp/Database.h>
#include "databasecreater.h"



int main () {

    std::string path_bkk = "/home/ubuntu/Projects/splitted/in/jobs.yml";

    // Open a database file in create/write mode
    SQLite::Database db("jobs.db3", SQLite::OPEN_READWRITE | SQLite::OPEN_CREATE);
    std::cout << "SQLite database file '" << db.getFilename().c_str() << "' opened successfully\n";

    // Create a new table with an explicit "id" column aliasing the underlying rowid
    db.exec("DROP TABLE IF EXISTS jobs");

    db.exec("CREATE TABLE jobs (id INTEGER PRIMARY KEY, EndExecTime FLOAT, \
                        Federation TEXT, \
                        InputFiles TEXT, \
                        JobGroup TEXT, \
                        JobType TEXT, \
                        LocalJobID TEXT, \
                        OutputFiles TEXT, \
                        RescheduleTime TEXT, \
                        Site TEXT, \
                        StartExecTime FLOAT, \
                        Status TEXT, \
                        SubmissionTime FLOAT, \
                        SystemPriority FLOAT, \
                        'TotalCPUTime(s)' FLOAT, \
                        UserPriority FLOAT, \
                        'WallClockTime(s)' FLOAT)");


    INSERT_JOBS_DATABASE(db, path_bkk);

    return 0;

}
