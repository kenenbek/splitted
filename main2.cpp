//
// Created by kenenbek on 27.03.17.
//
#include <iostream>
#include <SQLiteCpp/SQLiteCpp.h>

int  main(int argc, char* argv[]){
    int count = 0;
    try {
        SQLite::Database    db("jobs.db3", SQLite::OPEN_READWRITE|SQLite::OPEN_CREATE);
        std::cout << "SQLite database file '" << db.getFilename().c_str() << "' opened successfully\n";

        SQLite::Statement   query(db, "SELECT * FROM jobs WHERE JobType == \"Turbo\"");
        std::cout << "SELECT * FROM test :\n";
        std::cout << query.getColumnCount() << std::endl;

        while (query.executeStep()) {
            ++count;
            std::cout << "row (" << query.getColumn(0) << ", \"" << query.getColumn(1) << "\")\n";
        }
    }
    catch (std::exception& e)
    {
        std::cout << "SQLite exception: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    std::cout << count << std::endl;

    return 0;
}

